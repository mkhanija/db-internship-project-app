import json
from flask_cors import CORS
from flask.ext import restful
from flask_rest_service import app, api
from scripts import constants
from flask.ext.restful import reqparse
CORS(app)


class app_data(restful.Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('ticker', type=str)
        super(app_data, self).__init__()

    def get(self):
        try:
            with open('data/data_all_companies.json') as file:
                data = json.load(file)
            return data
        except Exception as exc:
            return {
                "status": "Server Error: " + str(exc)
            }

    def post(self):
        args = self.parser.parse_args()
        ticker = args['ticker']
        if ticker:
            try:
                with open('data/past_seven_days_data_all_companies.json') as file:
                    data = json.load(file)

                all_companies = data[constants.app_data]
                company_data = {}
                company_found = False
                for company in all_companies:
                    if company[constants.app_data_company_ticker] == ticker:
                        company_data = company
                        company_found = True

                if company_found:
                    return {
                        constants.app_data:
                        [
                            company_data
                        ]
                    }
                else:
                    return {
                        "status": "Ticker '" + ticker + "' could not be found"
                    }
            except Exception as exc:
                return {
                    "status": "Server Error: " + str(exc)
                }
        else:
            return {
                'status': "Exception: 'ticker' was not defined"
            }


api.add_resource(app_data, '/data-all-companies')
