import datetime
import time
from scripts.fetch_all_data import fetch_all_data
from apscheduler.schedulers.blocking import BlockingScheduler

sched = BlockingScheduler()

# Schedule for four hours ahead
# TODO: Commit this code if code running works
@sched.scheduled_job('interval', hours=3, start_date='2019-07-23 17:55:00', end_date='2019-08-02 12:00:00')
def some_scheduler():
    print("=============================================================== ****")
    day_of_week = datetime.datetime.today().weekday()
    if day_of_week < 5:
        print("**** Preparing to fetch data on " + str(datetime.datetime.today()) + "...")

        start_time = time.time()
        try:
            fetch_all_data()
            print("**** Successfully fetched all data on " + str(datetime.datetime.today()) + "... ****")

        except Exception as exc:
            print("Exception occurred when fetching data: " + str(exc))

        end_time = time.time()
        print("**** Total execution time: " + str(end_time - start_time) + " seconds. ****")
        print("===============================================================")


sched.start()
