import scrapy
import re
from scrapy.crawler import CrawlerProcess
from scrapy.spiders import XMLFeedSpider
from scripts import functions, constants
import scrapy.crawler as crawler
from multiprocessing import Process, Queue
from twisted.internet import reactor

outputResponse = []
search_results = []

class WsjSpider(scrapy.Spider):
    itertag = 'headline-container'
    company_name = ''
    results = []
    name = "wsj_spider"
    # start_urls = []

    def __init__(self, companies=[], **kwargs):
        print("========IN THE SPIDER============")
        self.start_urls = generate_start_urls(companies)

    # def start_requests(self):
    #     search_url = 'https://www.wsj.com/search/term.html?KEYWORDS=' + self.company_name + '&min-date=2019/07/01&max-date=2019/07/25&isAdvanced=true&daysback=90d' \
    #                                                                                         '&andor=AND&sort=date-desc&source=wsjarticle,wsjblogs,wsjvideo,interactivemedia,sitesearch,wsjpro'

        # yield scrapy.Request(url=search_url, callback=self.parse)
    "default parse function it contains all the the parsing methods" \
    "it parse every search result link"
    def parse(self, response):
        print("==================IN THE MAIN PARSING=================")

        search_result = self.parse_result_link(response)
        search_result = list(search_result)

        i = 0;
        for e in search_result:
            print(i)
            print(e)
            i += 1

    "this method parse the given search link" \
    "and return a list with the links to every search result related to a" \
    "company"
    def parse_result_link(self, response):
        print("============IN PARSE RESULT LINK FOR A COMPANY=================")
        # temp_link_results = []
        for result in response.xpath("//div[@class='headline-container']"):

            "extract the elements from the website and stores it"
            art_name = result.xpath('.//h3[@class="headline"]/a/text()').extract_first()
            date_today = result.xpath('.//time[@class="date-stamp-container highlight"]/text()').extract_first()
            date_other = result.xpath('.//time[@class="date-stamp-container"]/text()').extract_first()
            art_summary = result.xpath('.//h3[@class="summary-container"]/text()').extract_first()
            url = result.xpath('.//h3[@class="headline"]/a/@href').extract_first()

            "Fixing the link do all of them start with www.wsj.com"
            pattern = 'https://www.wsj.com'
            if not re.search(pattern, url):
                fix_url = pattern + url
            else:
                fix_url = url

            # self.l.add_value('article_title', art_name)
            # self.l.add_value('article_url', fix_url)

            # control array to verify results
            print(art_name)
            search_results.append([
                art_summary, art_name, fix_url
            ])
            # outputResponse.append([art_name, fix_url])

        #Move to the next page in the search result
        NEXT_PAGE_SELECTOR = './/li[@class="next-page"]/a/@href'
        next_page_ending = response.xpath(NEXT_PAGE_SELECTOR).extract_first()
        if next_page_ending:
            next_page = "https://www.wsj.com/search/term.html" + next_page_ending
            # temp_link_results = self.parse_search_link(next_page)
            yield scrapy.Request(
                next_page,
                callback=self.parse_result_link()
            )

        # item = {}
        # "List use to store all the search results "
        # " every resuls is saved as ['Title','url','article summary'] "
        #
        # "extract the elements from the website and stores it"
        # item['art_summary'] = item.xpath('.//h3[@class="summary-container"]/text()').extract_first()
        # art_name = item.xpath('.//h3[@class="headline"]/a/text()').extract_first()
        # item['date_today'] = item.xpath('.//time[@class="date-stamp-container highlight"]/text()').extract_first()
        # item['date_other'] = item.xpath('.//time[@class="date-stamp-container"]/text()').extract_first()
        # item['url'] = item.xpath('.//h3[@class="headline"]/a/@href').extract_first()
        #
        # print(item)
        # "Fixing the link do all of them start with www.wsj.com"
        # pattern = 'https://www.wsj.com'
        # if not re.search(pattern, item['url']):
        #     fix_url = pattern + item['url']
        # else:
        #     fix_url = item['url']
        #
        # # yield scrapy.Request( url=fix_url, callback=self.parse_article_info)
        #
        # # self.l.add_value('article_title', art_name)
        # # self.l.add_value('article_url', fix_url)
        #
        # # control array to verify results
        # outputResponse.append(item)
        #
        # # #Move to the next page in the search result
        # # NEXT_PAGE_SELECTOR = './/li[@class="next-page"]/a/@href'
        # # next_page_ending = response.xpath(NEXT_PAGE_SELECTOR).extract_first()
        # # if next_page_ending:
        # #     next_page = "https://www.wsj.com/search/term.html" + next_page_ending
        # #
        # #     yield scrapy.Request(
        # #         next_page,
        # #         callback=self.parse
        # #     )
        # print("==============================")
        # print(item)
        # # return item


    " This function returns a list "
    # def parse_results_url(self, response):
    # def parse_article_info(self, response):
    #     article_title = response.xpath('.//div[@class="wsj-article-headline-wrap"]/h1/text()').extract_first()
    #     date = response.xpath('.//time[@class="timestamp article__timestamp flexbox__flex--1"]/text()').extract_first()
    #     article_summary = response.xpath('.//div[@class="wsj-snippet-body"]/p/text()').extract_first()
    #
    #     outputResponse.append([article_summary, article_title, date])

#function use to create a list containing all the start links to retrieve search results"
def generate_start_urls(companies):
    print("*****************GENERATING START URLS****************************")
    start_urls = []
    for company in companies:
        # start_urls.append('https://www.wsj.com/search/term.html?KEYWORDS=' + company[0] + '&min-date=2019/07/01&max-date=2019/07/25&isAdvanced=true&daysback=90d&andor=AND&sort=date-desc&source=wsjarticle,wsjblogs,wsjvideo,interactivemedia,sitesearch,wsjpro')
        start_urls.append('https://www.wsj.com/search/term.html?KEYWORDS=' + company[0] + '&mod=searchresults_viewallresults')

    print(start_urls)
    return start_urls

# def get_search_results_links()

def get_all_companies_info(companies):
    process = CrawlerProcess()
    process.crawl(WsjSpider, companies=companies)
    process.start()

    print(outputResponse)
    # for article in outputResponse:
    #     article[0] = functions.clean_text(str(article[0]))

    # temp_results = outputResponse
    # print(temp_results)
    # outputResponse.clear()

    # print("============GETTING SENTIMENT======")
    # print(temp_results)
    # return functions.sentiment_from_all_data(temp_results)


if __name__ == '__main__':
    get_all_companies_info(constants.companies)

    # print(len(total_results[0]))
    # print(total_results[0])
    # print(len(total_results[1]))
    # print(total_results[1])
    # print(len(total_results[2]))
    # print(total_results[2])

