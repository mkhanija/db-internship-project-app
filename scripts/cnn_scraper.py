import requests
import datetime
from scripts import functions
from scripts import constants


def analyze_article_sentiment(text):
    return functions.sentiment_from_all_data(text)


# method used to scrape cnn search results"
# @return results an array that contain a list of elements in the following order "
#        ['url', 'article']"
def cnn_crawler(company):
    "stores the search results from the CNN website"
    results = []
    company_name = company

    # creating url
    url = 'https://search.api.cnn.io/content?size=100&q=' + company_name + '&sort=newest'

    # get request, retrieve information about the given company name
    r = requests.get(url)
    data = r.json()

    # retrieve the total number of search results in the json object
    total_number_of_articles = data['meta']['of']

    number_of_results = 0
    # fix the url to send a get request that will return all the search results
    company_name = clean_company_name(company_name)

    # &category=business
    page_url = 'https://search.api.cnn.io/content?size=100&q=' + company_name + '&sort=newest&from=' + str(number_of_results)

    while number_of_results < total_number_of_articles:
        print(page_url)
        r = requests.get(page_url)
        complete_data = r.json()

        for result in complete_data['result']:
            date = format_date(result['lastModifiedDate'])

            if date < get_stop_date():
                return results

            results.append([
                functions.clean_text(result['body']),
                date,
                result['url']
            ])

        # 100 is the total number of search result per page
        # this is moving through pages
        number_of_results += 100
        page_url = 'https://search.api.cnn.io/content?size=100&q=' + company_name + '&sort=newest&from=' + str(
            number_of_results)

    return results

def clean_company_name(company):
    if company.__contains__(' '):
        company = company.replace(' ', '%20')

    company = str(company).lower()

    return company

def format_date(article_date):
    year = int(article_date[0:4])
    month = int(article_date[5:7])
    day = int(article_date[8:10])

    return datetime.datetime(year, month, day).strftime('%Y-%m-%d')


def get_stop_date():
    return functions.get_past_date('%Y-%m-%d', 7)


def split_articles_by_day(week_of_articles):
    list_by_day = {}
    if len(week_of_articles) > 0:
        current_date = week_of_articles[0][1]
        articles_this_day = []
        # 1 = date, 0 = article
        for result in week_of_articles:
            if result[1] == current_date:
                articles_this_day.append([result[0]])
            else:
                list_by_day[current_date] = articles_this_day
                articles_this_day = []
                articles_this_day.append([result[0]])
                current_date = result[1]

        list_by_day[current_date] = articles_this_day

    return list_by_day


def get_weekly_sentiment_all_companies():
    sentiment_per_company = []

    for company in constants.companies:
        company_name = str(company[0]).lower()
        this_company_results = cnn_crawler(company_name)
        dict_of_articles = split_articles_by_day(this_company_results)

        print('Company: ' + company_name)

        sentiment_for_date = []
        for (date, articles_list) in dict_of_articles.items():
            sentiment_for_date.append({
                constants.app_data_date: date,
                constants.app_data_sentiment: functions.sentiment_from_all_data(articles_list)
            })

        sentiment_per_company.append([company_name, sentiment_for_date])

    return sentiment_per_company


