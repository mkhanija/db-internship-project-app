import requests
from scripts import constants, functions


def get_stock_price_change(open_price, close_price):
    return round(((close_price / open_price) - 1.00) * 100.00, 2)


def get_daily_stocks_since_date(ticker, outputsize, datatype, num_days_ago):
    params = {
        'function': constants.alpha_vantage_function_daily,
        'symbol': ticker,
        'outputsize': outputsize,
        'datatype': datatype,
        'apikey': constants.alpha_vantage_api_key
    }

    all_data = requests.get(constants.alpha_vantage_api_url, params=params).json()

    if constants.alpha_vantage_meta_data in all_data:
        last_refreshed = all_data[constants.alpha_vantage_meta_data]['3. Last Refreshed']
    else:
        raise KeyError("Error in Alpha Vantage return data. Data: " + str(all_data))

    stocks_data = all_data[constants.alpha_vantage_time_series_daily]
    stocks_since_date = []

    for days_to_subtract in range(0, num_days_ago):
        date = functions.get_past_date('%Y-%m-%d', days_to_subtract)
        if date in stocks_data:
            stock = stocks_data[date]
            data = {
                constants.app_data_date: date,
                constants.app_data_company_stocks_open: stock['1. open'],
                constants.app_data_company_stocks_high: stock['2. high'],
                constants.app_data_company_stocks_low: stock['3. low'],
                constants.app_data_company_stocks_close: stock['4. close'],
                constants.app_data_company_stocks_change: str(get_stock_price_change(float(stock['1. open']), float(stock['4. close']))),
                constants.app_data_company_stocks_volume: stock['5. volume']
            }

            stocks_since_date.append(data)

    return {
        constants.app_data_company_stocks_last_refreshed: last_refreshed,
        constants.app_data_stocks: stocks_since_date
    }


def daily_stocks_past_seven_days(ticker, outputsize, datatype):
    # Has to be 8 because we need to account for today.
    return get_daily_stocks_since_date(ticker, outputsize, datatype, 8)


def today_stock_data(ticker, outputsize, datatype):
    return get_daily_stocks_since_date(ticker, outputsize, datatype, 1)
