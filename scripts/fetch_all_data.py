# This file will fetch and analyze all the necessary data from twitter and our new media sources
# and it will write all the data to a json file
import json
import time
from scripts import alpha_vantage_api, constants, twitter_scrapper, cnn_scraper, google_news_scraper


def _get_todays_data_twitter_all_companies():
    all_companies = []
    alpha_max_requests_per_min = constants.alpha_vantage_max_num_request_per_min
    alpha_sleep_time = 70  # seconds
    number_tweets_per_company = 200

    for company in constants.companies:
        # We must wait one minute every 5 calls because of Alpha_Vantage limitations
        if len(all_companies) % alpha_max_requests_per_min == 0 and len(all_companies) > 0:
            time.sleep(alpha_sleep_time)
        # elif len(all_companies) % twitter_max_requests_per_15_min == 0 and len(all_companies) > 0:
            # time.sleep(twitter_sleep_time)

        company_name = company[0]
        company_ticker = company[1]
        company_keyword = '#' + company[2]

        print('Twitter daily data: ' + company_name)

        todays_stocks = alpha_vantage_api.today_stock_data(company_ticker, constants.alpha_vantage_outputsize_compact,
                                                           constants.alpha_vantage_datatype_json)

        company_data = {
            constants.app_data_company_name: company_name,
            constants.app_data_company_ticker: company_ticker,
            constants.app_data_company_stocks_last_refreshed: todays_stocks[constants.app_data_company_stocks_last_refreshed],
            constants.app_data_company_stocks: todays_stocks[constants.app_data_stocks],
            constants.app_data_twitter_sentiment:
                twitter_scrapper.get_sentiment_tweets_today(company_keyword, number_tweets_per_company),
            constants.app_data_media_sentiment: get_media_sentiment(company_name)
        }

        all_companies.append(company_data)

    return all_companies


def _get_data_past_seven_days_twitter_all_companies():
    all_companies = []
    alpha_max_requests_per_min = constants.alpha_vantage_max_num_request_per_min
    alpha_sleep_time = 70  # seconds
    number_tweets_per_company = 200

    for company in constants.companies:
        # We must wait one minute every 5 calls because of Alpha_Vantage limitations
        if len(all_companies) % alpha_max_requests_per_min == 0 and len(all_companies) > 0:
            time.sleep(alpha_sleep_time)
        # elif len(all_companies) % twitter_max_requests_per_15_min == 0 and len(all_companies) > 0:
            # time.sleep(twitter_sleep_time)

        company_name = company[0]
        company_ticker = company[1]
        company_keyword = '#' + company[2]

        print('Twitter Past 7 days: ' + company_name)

        stocks = alpha_vantage_api.daily_stocks_past_seven_days(company_ticker,
                                                                constants.alpha_vantage_outputsize_compact,
                                                                constants.alpha_vantage_datatype_json)
        company_data = {
            constants.app_data_company_name: company_name,
            constants.app_data_company_ticker: company_ticker,
            constants.app_data_company_stocks_last_refreshed: stocks[constants.app_data_company_stocks_last_refreshed],
            constants.app_data_company_stocks: stocks[constants.app_data_stocks],
            constants.app_data_twitter_sentiment:
                twitter_scrapper.get_sentiment_tweets_past_week(company_keyword, number_tweets_per_company),
            constants.app_data_media_sentiment: get_media_sentiment(company_name)
        }

        all_companies.append(company_data)

    return all_companies


def _get_all_google_articles():
    return google_news_scraper.get_all_companies_sentiment()


def _get_cnn_weekly_articles_and_sentiments():
    return cnn_scraper.get_weekly_sentiment_all_companies()


def _get_google_sentiment(company_name):
    company_name = str(company_name).lower()
    found = False
    try:
        with open('../data/data_google.json') as file:
            all_companies_data = json.load(file)
        all_companies_array = all_companies_data['app_data']

        for company in all_companies_array:
            name = str(company[0]).lower()
            if name == company_name:
                # Return sentiment of company
                found = True
                return company[1]

        if not found:
            return []
    except Exception as exc:
        raise Exception("Error when opening file data_google.json: " + str(exc))


def _get_cnn_sentiment(company_name):
    company_name = str(company_name).lower()
    found = False
    with open('../data/data_cnn_scrapper.json') as file:
        all_companies_data = json.load(file)

    company_sentiment = []
    for company in all_companies_data['app_data']:
        name = str(company[0]).lower()
        if name == company_name:
            found = True
            company_sentiment = company[1]
            # Break out of loop, found company
            break

    if found:
        return company_sentiment
    else:
        return []


def get_media_sentiment(company_name):
    cnn_data = _get_cnn_sentiment(company_name)
    google_data = _get_google_sentiment(company_name)

    # Combine data
    media_sentiment = []
    for google_item in google_data:
        found_date = False
        google_date = google_item['date']
        google_sentiment = google_item['sentiment']

        for cnn_item in cnn_data:
            cnn_date = cnn_item['date']
            cnn_sentiment = cnn_item['sentiment']

            # Date exists on both Google and CNN objects, combine them.
            if google_date == cnn_date:
                found_date = True
                overall_sentiment = []

                overall_count_positives = google_sentiment[1][1] + cnn_sentiment[1][1]
                overall_sentiment.append([constants.string_count_positives, overall_count_positives])

                overall_count_neutrals = google_sentiment[2][1] + cnn_sentiment[2][1]
                overall_sentiment.append([constants.string_count_neutrals, overall_count_neutrals])

                overall_count_negatives = google_sentiment[3][1] + cnn_sentiment[3][1]
                overall_sentiment.append([constants.string_count_negatives, overall_count_negatives])

                texts_analyzed_counts = overall_count_positives + overall_count_neutrals + overall_count_negatives

                max_sentiment = [0, 0]
                for sentiment in overall_sentiment:
                    if sentiment[1] > max_sentiment[1]:
                        if sentiment[0] == constants.string_count_positives:
                            max_sentiment = [constants.string_positive, sentiment[1]]
                        elif sentiment[0] == constants.string_count_neutrals:
                            max_sentiment = [constants.string_neutral, sentiment[1]]
                        elif sentiment[0] == constants.string_count_negatives:
                            max_sentiment = [constants.string_negative, sentiment[1]]

                if max_sentiment[0] == constants.string_positive:
                    max_sentiment = [constants.string_overall_sentiment, constants.string_positive]
                elif max_sentiment[0] == constants.string_neutral:
                    max_sentiment = [constants.string_overall_sentiment, constants.string_neutral]
                elif max_sentiment[0] == constants.string_negative:
                    max_sentiment = [constants.string_overall_sentiment, constants.string_negative]

                new_sentiment = [max_sentiment,
                                 [constants.string_count_positives, overall_count_positives],
                                 [constants.string_count_neutrals, overall_count_neutrals],
                                 [constants.string_count_negatives, overall_count_negatives],
                                 [constants.texts_analyzed, texts_analyzed_counts]]

                new_json = {
                    constants.app_data_date: google_date,
                    constants.app_data_sentiment: new_sentiment
                }

                media_sentiment.append(new_json)
                break

        if not found_date:
            media_sentiment.append(google_item)

    return media_sentiment


def fetch_and_save_daily_twitter_data():
    app_data = {'app_data': _get_todays_data_twitter_all_companies()}
    with open('../data/data_all_companies.json', 'w') as file:
        json.dump(app_data, file)


# Will get twitter sentiment, media sentiment, and stocks data for EACH
# company within the past 7 days.
def fetch_and_save_weekly_twitter_data():
    app_data = {'app_data': _get_data_past_seven_days_twitter_all_companies()}
    with open('../data/past_seven_days_data_all_companies.json', 'w') as file:
        json.dump(app_data, file)


def fetch_and_save_google_articles():
    app_data = {'app_data': _get_all_google_articles()}
    with open('../data/data_google.json', 'w') as file:
        json.dump(app_data, file)


def fetch_and_save_cnn_articles():
    app_data = {'app_data': _get_cnn_weekly_articles_and_sentiments()}
    with open('../data/data_cnn_scrapper.json', 'w') as file:
        json.dump(app_data, file)


def fetch_all_data():
    # IMPORTANT: MUST BE RAN IN THIS ORDER!!
    print('--------------------- Fetching Google News data and saving it --------------------------------')
    # fetch_and_save_google_articles()
    print('-------- Finished getting articles Google data, now getting CNN Articles ---------------------')
    # fetch_and_save_cnn_articles()
    print('-------- Finished getting CNN data, now getting in weekly Twitter data -----------------------')
    fetch_and_save_weekly_twitter_data()
    print('-------- Finished getting weekly Twitter data, now getting in daily Twitter data -------------')
    fetch_and_save_daily_twitter_data()
    print('-------- Finished getting daily Twitter data. Finished getting all data ----------------------')


if __name__ == '__main__':
    fetch_all_data()
    # test = get_media_sentiment('verizon')
    # print('============================')
    # for item in test:
    #     print(item)
    #
    # #fetch_all_data()
