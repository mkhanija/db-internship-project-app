# Twitter credentials
twitter_consumer_key = 'NAT4fYaVVBLBS68FCJIVkIOa6'
twitter_consumer_secret = 'wEyEtllSaCXJ3LKd0HndKPrmXzK4LptZMVyByKf3zmB6aCgQ7q'
twitter_access_key = '1141725907868164096-b1u9n1ZuJfSA5gn0ijVpW528pglkNA'
twitter_access_secret = 'bLatjroQGzpfvm2yzW7c5S41m4wkMreBioUB6h4QHXX3L'
twitter_num_tweets_per_min = 100
twitter_num_requests_per_15_min = 15

# MonkeyLearn credentials
monkeylearn_api_key = 'af531d8ca6d549d3ef989d52d8f1e9c6afa7f11d'
monkeylearn_twitter_model_id = 'cl_qkjxv9Ly'
monkeylearn_generic_model_id = 'cl_pi3C7JiL'

# Alpha Vantage API, API only allows 5 calls per minute
alpha_vantage_api_key = '20O4HZGA3CXKK16D'
alpha_vantage_max_num_request_per_min = 5
alpha_vantage_api_url = 'https://www.alphavantage.co/query'
alpha_vantage_function_daily = 'TIME_SERIES_DAILY'
alpha_vantage_function_daily_adjusted = 'TIME_SERIES_DAILY_ADJUSTED'
alpha_vantage_function_weekly = 'TIME_SERIES_WEEKLY'
alpha_vantage_function_weekly_adjusted = 'TIME_SERIES_WEEKLY_ADJUSTED'
alpha_vantage_function_monthly = 'TIME_SERIES_MONTHLY'
alpha_vantage_function_monthly_adjusted = 'TIME_SERIES_MONTHLY_ADJUSTED'
alpha_vantage_interval_1min = '1min'
alpha_vantage_interval_5min = '5min'
alpha_vantage_interval_15min = '15min'
alpha_vantage_interval_30min = '30min'
alpha_vantage_interval_60min = '60min'
alpha_vantage_outputsize_compact = 'compact'
alpha_vantage_outputsize_full = 'full'
alpha_vantage_datatype_json = 'json'
alpha_vantage_datatype_csv = 'csv'
alpha_vantage_meta_data = 'Meta Data'
alpha_vantage_time_series_daily = 'Time Series (Daily)'

# App Data JSON object attributes
app_data = 'app_data'
app_data_company_name = 'company_name'
app_data_company_ticker = 'company_ticker'
app_data_company_stocks_last_refreshed = 'company_stocks_last_refreshed'
app_data_company_stocks = 'company_stocks'
app_data_stocks = 'stocks'
app_data_date = 'date'
app_data_company_stocks_open = 'open'
app_data_company_stocks_high = 'high'
app_data_company_stocks_low = 'low'
app_data_company_stocks_close = 'close'
app_data_company_stocks_change = 'change'
app_data_company_stocks_volume = 'volume'
app_data_twitter_sentiment = 'twitter_sentiment'
app_data_sentiment = 'sentiment'
app_data_media_sentiment = 'media_sentiment'


# Sentiment Constants
string_negative = 'negative'
string_neutral = 'neutral'
string_positive = 'positive'

string_overall_sentiment = 'overall_sentiment'
string_count_positives = 'count_positives'
string_count_neutrals = 'count_neutrals'
string_count_negatives = 'count_negatives'

texts_analyzed = 'texts_analyzed'

# Companies list. First index is company name, second is company
# ticker and third index is keyword to search for in twitter tweets
companies = [['Apple', 'AAPL', 'apple'],
             ['Microsoft', 'MSFT', 'microsoft'],
             ['Walmart', 'WMT', 'walmart'],
             ['Exxon Mobil', 'XOM', 'exxonmobil'],
             ['Amazon', 'AMZN', 'amazon'],
             ['CVS', 'CVS', 'cvs'],
             ['AT&T', 'T', 'att'],
             ['General Motors', 'GM', 'gm'],
             ['United Health Group', 'UNH', 'unitedhealthgroup'],
             ['Chevron', 'CVX', 'chevron'],
             ['Costco Wholesale', 'COST', 'costco'],
             ['JPMorgan Chase', 'JPM', 'jpmorgan'],
             ['Verizon', 'VZ', 'verizon'],
             ['General Electric', 'GE', 'generalelectric'],
             ['Boeing', 'BA', 'boeing'],
             ['Wells Fargo', 'WFC', 'wellsfargo'],
             ['IBM', 'IBM', 'ibm'],
             ['Lenovo Group Limited', 'LNVGY', 'lenovo'],
             ['Urban Outfitters, Inc.', 'URBN', 'urbanoutfitters'],
             ['Google', 'GOOGL', 'google'],
             ['Chipotle', 'CMG', 'chipotle'],
             ['Snapchat', 'SNAP', 'snapchat'],
             ['Facebook', 'FB', 'facebook'],
             ['Deutsche Bank', 'DB', 'deutschebank'],
             ['Capital One Financial Corp.', 'COF', 'capitalone']]

companies_to_add = [['Lenovo Group Limited', 'LNVGY', 'lenovo'],
                    ['Urban Outfitters, Inc.', 'URBN', 'urbanoutfitters'],
                    ['Goole', 'GOOGL', 'google']]
