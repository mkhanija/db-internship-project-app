import time
import requests
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scripts import constants, wsj_scrapper, google_news_scraper


def get_csv_from_alpha_vantage(ticker):
    params = {
        'function': constants.alpha_vantage_function_daily,
        'symbol': ticker,
        'outputsize': constants.alpha_vantage_outputsize_full,
        'datatype': constants.alpha_vantage_datatype_csv,
        'apikey': constants.alpha_vantage_api_key
    }

    try:
        return requests.get(constants.alpha_vantage_api_url, params=params).content
    except Exception as exc:
        return Exception("Exception: " + str(exc))


def gather_stocks_data():
    sleep_time = 60  # seconds
    num_companies_retrieved = 0

    for company in constants.companies:
        ticker = str(company[1]).lower()
        # Sleep for 1 minute because api only allows 5 calls / min
        if num_companies_retrieved % 5 == 0 and num_companies_retrieved is not 0:
            print("Sleeping process for " + str(sleep_time) + " seconds.")
            time.sleep(sleep_time)

        csv_contents = get_csv_from_alpha_vantage(ticker)
        try:
            path = '../data/company_stocks/' + ticker + '.csv'
            print("Writing to file: " + ticker + ".csv")
            with open(path, 'wb') as file:
                file.write(csv_contents)
            print("Fetched csv file for: " + ticker)
            print("=======================================================")
        except Exception as exc:
            return Exception("Exception Occurred: " + str(exc))

        num_companies_retrieved += 1


def read_csv(file):
    return pd.read_csv(file)


def get_percentage_of_data(data_length, percentage):
    return math.ceil(data_length * percentage)



def get_train_and_test(file):
    # Get 85% of total data
    data = read_csv(file)
    data_85_percent = get_percentage_of_data(len(data), 0.85)
    train = data[:data_85_percent]
    test = data[data_85_percent:]

    return train, test


if __name__ == '__main__':
    file = '../data/company_stocks/aapl.csv'
    # get_train_and_test(file)

    # print(google_news_scraper.get_sentiment_article_text('Google'))

    # """
    for company in constants.companies:
        print('================')
        print('Getting data for : ' + company[0])
        print(wsj_scrapper.get_sentiment_article_text(company[0]))
    # """


    # plt.plot(data['close'], label='TESTING')
    # plt.show()
