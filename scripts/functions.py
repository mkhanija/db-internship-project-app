import re
from datetime import datetime, timedelta
from textblob import TextBlob
from langdetect import detect
from afinn import Afinn
from scripts import constants
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from monkeylearn import MonkeyLearn


def clean_text(text):
    text = re.sub(r'http\S+', '', text, flags=re.MULTILINE)
    text = text.encode('ascii', 'ignore').decode('ascii')
    text = re.sub('[^A-Za-z0-9 ]+', '', text)
    text = text.lower()

    return text


def detect_language(text):
    return detect(text)


def write_to_csv(data, file_name):
    with open(file_name, 'a') as csv_file:
        for row in data:
            for column in row:
                csv_file.write(str(column))
                csv_file.write(',')

            csv_file.write('\n')


def analyze_sentiment_twitter_monkeylearn(text):
    ml = MonkeyLearn(constants.monkeylearn_api_key)
    data = [text]
    result = ml.classifiers.classify(constants.monkeylearn_twitter_model_id, data)
    return result.body


def analyze_sentiment_vader(text):
    # use to analyze social media
    analyzer = SentimentIntensityAnalyzer()
    vs = analyzer.polarity_scores(text)

    return vs


def analyze_sentiment_afinn(text):
    # Use to analyze non-social media posts
    # Use 'score_with_pattern' as it uses word patterns
    # and thus is more accurate
    return Afinn().score_with_pattern(text)


# Returns array with how many tweets are positive, negative, or neutral,
# and how many tweets were analyzed in total. parameter must be 2d array of arrays
# of arrays. EX: [['sentiment', score], ['sentiment', score]]
# NOTE: array must be 2d array of array with the first entry of each array
# being the text to analyze[['text to analzye'],[],..., []],...[['text', [],...,[]]]
def sentiment_from_all_data(array):
    positive_count = 0
    neutral_count = 0
    negative_count = 0
    for row in array:
        text = row[0]
        sentiment = analyze_sentiment(text)

        if sentiment == constants.string_positive:
            positive_count = positive_count + 1

        elif sentiment == constants.string_neutral:
            neutral_count = neutral_count + 1

        elif sentiment == constants.string_negative:
            negative_count = negative_count + 1

    all_sentiments = [[constants.string_count_positives, positive_count],
                      [constants.string_count_neutrals, neutral_count],
                      [constants.string_count_negatives, negative_count]]

    max_sentiment = ['', 0]
    for sentiment_score in all_sentiments:
        if sentiment_score[1] > max_sentiment[1]:
            max_sentiment = sentiment_score

    # Setting value of max sentiment
    if max_sentiment[0] == constants.string_count_positives:
        max_sentiment[0] = constants.string_positive

    elif max_sentiment[0] == constants.string_count_neutrals:
        max_sentiment[0] = constants.string_neutral

    elif max_sentiment[0] == constants.string_count_negatives:
        max_sentiment[0] = constants.string_negative

    return [[constants.string_overall_sentiment, max_sentiment[0]],
            [constants.string_count_positives, positive_count],
            [constants.string_count_neutrals, neutral_count],
            [constants.string_count_negatives, negative_count],
            [constants.texts_analyzed, len(array)]]


def analyze_sentiment_textblob(text):
    # Polarity has range of [-1, 1].
    # Subjectivity has range [0.0, 1.0]
    # where 0 is very objective and 1 is very subjective
    return TextBlob(text).sentiment


def analyze_sentiment(text):
    all_scores = []

    # Get Afinn sentiment score
    afinn_score = analyze_sentiment_afinn(text)
    all_scores.append(afinn_score)

    # Get TextBlob sentiment score
    textblob_score = analyze_sentiment_textblob(text).polarity
    all_scores.append(textblob_score)

    # Get sentiment analysis from Monkeylearn
    # TODO: Create another account with MonkeyLearn?
    # TODO: Comment out all code, including last line at the end
    # TODO: where monkeylearn_confidence is added to overall sentiment
    """
    monkeylearn_classification = (analyze_sentiment_twitter_monkeylearn(text))[0]['classifications'][0]['tag_name']
    monkeylearn_confidence = (analyze_sentiment_twitter_monkeylearn(text))[0]['classifications'][0]['confidence']

    if monkeylearn_classification == constants.negative:
        monkeylearn_confidence = monkeylearn_confidence # * -0.5
    elif monkeylearn_classification == constants.positive:
        monkeylearn_confidence = monkeylearn_confidence # * 0.5
    all_scores.append(monkeylearn_confidence)
    """

    # Get Vader classification and confidence
    vader_scores = analyze_sentiment_vader(text)
    vader_classifications = [[constants.string_negative, vader_scores['neg']],
                             [constants.string_neutral, vader_scores['neu']],
                             [constants.string_positive, vader_scores['pos']]]
    vader_max_score = [0, 0]
    for score in vader_classifications:
        if score[1] > vader_max_score[1]:
            if vader_max_score[0] == constants.string_negative:
                vader_max_score = score[1] * -1

    if vader_max_score[0] == constants.string_negative:
        vader_max_score[1] = vader_max_score[1] * -0.90
    elif vader_max_score[0] == constants.string_positive:
        vader_max_score[1] = vader_max_score[1] * 0.90

    all_scores.append(vader_max_score[1])

    overall_score = afinn_score + textblob_score + vader_max_score[1]  # + monkeylearn_confidence
    all_scores.append(overall_score)

    if overall_score < -0.25:
        return constants.string_negative
    elif -0.25 <= overall_score < 0.25:
        return constants.string_neutral
    elif overall_score > 0.25:
        return constants.string_positive

    return overall_score


# ex: '%Y-%m-%d'
def get_past_date(date_format, days_to_subtract):
    week_ago = datetime.today() - timedelta(days=days_to_subtract)
    return week_ago.strftime(date_format)
