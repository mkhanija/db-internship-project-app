import tweepy
import math
import datetime
# import numpy as np
from scripts import functions
from scripts import constants

# Functions can return numpy arrays ONLY for testing
# Purposes, change back to regular arrays before publishing to server
# as numpy arrays cannon easily be converted to json objects.


# For most endpoints, this is 15 requests per 15 minutes. (1500 tweets per 15 min)
# So if you have set the global sleep_on_rate_limit to True,
# the process looks something like this:
def get_tweets(search_keyword, num_tweets, until):
    # Creating the API end point
    auth = tweepy.OAuthHandler(constants.twitter_consumer_key, constants.twitter_consumer_secret)
    api = tweepy.API(auth)

    all_tweets = []
    max_id = None

    # Loop will run for as many times as it needs to in order to get
    # specified number of tweets. Twitter only allows 100 tweets per request so
    # this was a work around
    remaining_tweets = num_tweets
    for x in range(0, math.ceil(remaining_tweets / constants.twitter_num_tweets_per_min)):
        if remaining_tweets > constants.twitter_num_tweets_per_min:
            temp = remaining_tweets - constants.twitter_num_tweets_per_min
            tweets_to_fetch = remaining_tweets - temp
            remaining_tweets = temp
        else:
            tweets_to_fetch = remaining_tweets
            remaining_tweets = remaining_tweets - remaining_tweets

        # Add parameters to get tweets from a week ago: since_id = '2018-01-01'
        for tweet in tweepy.Cursor(api.search, wait_on_rate_limit=True, until=until,
                                   q=search_keyword + " -filter:retweets", max_id=max_id, lang='en',
                                   tweet_mode='extended').items(tweets_to_fetch):
            tweet_date = datetime.datetime.strptime(str(tweet.created_at), '%Y-%m-%d %H:%M:%S')
            tweet_info = [functions.clean_text(tweet.full_text), str(tweet.id), str(tweet_date.date())]
            all_tweets.append(tweet_info)

        if len(all_tweets) > 0:
            max_id = int(all_tweets[len(all_tweets) - 1][1]) - 1

    return all_tweets
    # return np.array(all_tweets)


def get_sentiment_tweets_since_date(search_keyword, num_tweets_per_day, num_days_ago):
    all_tweets = []
    all_dates_past_week = []
    all_sentiments = []

    for i in range(0, num_days_ago):
        # Converting date from week ago to datetime object
        date = datetime.datetime.strptime(functions.get_past_date('%Y-%m-%d', i), '%Y-%m-%d')

        # Add one day because Twitter api gets tweets for date before date provided, excluding tweets
        # from the date provided. If date provided is 2019-12-24, then twitter api gets tweets for date
        # 2019-12-23 and anytime before that.s
        new_date = date + datetime.timedelta(days=1)

        # Save dates for which we are getting tweets
        all_dates_past_week.append(str(date.date()))

        for tweet in get_tweets(search_keyword, num_tweets_per_day, str(new_date.date())):
            all_tweets.append(tweet)

    for date in all_dates_past_week:
        tweets_for_this_date = []
        for tweet in all_tweets:
            if date == str(tweet[2]):
                tweets_for_this_date.append(tweet)

        data = {
            'date': date,
            'sentiment': functions.sentiment_from_all_data(tweets_for_this_date)
        }

        all_sentiments.append(data)

    return all_sentiments
    # return np.array(all_sentiments)


def get_sentiment_tweets_past_week(search_keyword, num_tweets_per_day):
    # Getting tweets from a week ago is 8 days
    return get_sentiment_tweets_since_date(search_keyword, num_tweets_per_day, 8)


def get_sentiment_tweets_today(search_keyword, num_tweets_per_day):
    return get_sentiment_tweets_since_date(search_keyword, num_tweets_per_day, 1)
