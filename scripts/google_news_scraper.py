from scrapy.spiders import XMLFeedSpider
from scrapy.crawler import CrawlerProcess
from scripts import functions, constants
from datetime import datetime, timedelta


# list that stores all the news article items
outputResponse = []


class GoogleNewsSpider(XMLFeedSpider):

    name = "google_news_spider"
    itertag = 'item'


    def __init__(self, companies=[], **kwargs):
        self.start_urls = _generate_start_urls(companies)

    def parse_node(self, response, node):
        item = {}

        item['company'] = str(response)[42:].replace('%20when%3A8d&hl=en-US&gl=US&ceid=US%3Aen', '').replace('%20', ' ').replace('>', '')
        item['description'] = node.xpath('description//text()').extract_first()
        item['title'] = node.xpath('title//text()',).extract_first()
        item['link'] =  node.xpath('link//text()').extract_first()
        item['pubDate'] = node.xpath('pubDate//text()').extract_first()
        item['source'] = node.xpath('source//text()').extract_first()
    
        #trimming the description
        item['description'] = _trim_description(item['description'])

        #trimming date
        item['pubDate'] = _reformat_date(item['pubDate'])
        
        #Appending list, making sure there are no empty descriptions
        if len(str(item['description']).replace(' ', '')) > 0:
            outputResponse.append(item)


def _trim_description(description):
    before, separator, description = description.partition('<p>')
    description = description.replace('</p>', "")
    return description


def _reformat_date(original_date):
    day, date, month, year, time, timezone = original_date.split(" ")
    month = month.upper()
    if month == 'JAN':
        month = '01'
    elif month == 'FEB':
        month = '02'
    elif month == 'MAR':
        month = '03'
    elif month == 'APR':
        month = '04'
    elif month == 'MAY':
        month = '05'
    elif month == 'JUN':
        month = '06'
    elif month == 'JUL':
        month = '07'
    elif month == 'AUG':
        month = '08'
    elif month == 'SEP':
        month = '09'
    elif month == 'OCT':
        month = '10'
    elif month == 'NOV':
        month = '11'
    elif month == 'DEC':
        month = '12'
    
    new_date = year + '-' + month + '-' + date
    return new_date


def _clean_company_name(company):
    if company.__contains__(' '):
        company = company.replace(' ', '%20')

    company = str(company).lower()

    return company


def _generate_start_urls(companies):
    start_urls = []

    for company in companies:
        start_urls.append('https://news.google.com/rss/search?q=' + _clean_company_name(company[0]) + '%20when%3A8d&hl=en-US&gl=US&ceid=US%3Aen')
    return start_urls


def _get_all_companies_info(companies):

    spider = GoogleNewsSpider()
    process = CrawlerProcess()
    process.crawl(spider, companies=companies)
    process.start()

    # sorting the list to latest first
    sortedOutputResponse = sorted(outputResponse, key=lambda k: k['pubDate'], reverse=True)

    all_articles = []

    for item in sortedOutputResponse:
        all_articles.append([functions.clean_text(item['description']), item['pubDate'], item['company']])

    return all_articles


def get_all_companies_sentiment():
    articles = _get_all_companies_info(constants.companies)

    all_articles_by_company = []

    for company in constants.companies:
        company_name = str(company[0]).lower()
        articles_per_company = []

        for article in articles:
            article_data = article[0]
            article_date = article[1]
            article_company_name = article[2]

            if article_company_name == company_name:
                articles_per_company.append([article_data, article_date])

        articles_per_date = []
        # sort by dates:
        days = generate_dates_list()
        articles_per_day = []

        print('Company: ' + company_name.upper())
        for day in days:
            for company_article in articles_per_company: #for 1 company
                if company_article[1] == day:
                    articles_per_day.append([company_article[0]])

            # calculate sentiment
            sentiment = functions.sentiment_from_all_data(articles_per_day)
            articles_per_date.append({
                constants.app_data_date: day,
                constants.app_data_sentiment: sentiment
            })

        all_articles_by_company.append([company_name, articles_per_date])

    return all_articles_by_company


def generate_dates_list():
    days = []
    
    days.append(str((datetime.now() - timedelta(0)).strftime('%Y-%m-%d')))
    days.append(str((datetime.now() - timedelta(1)).strftime('%Y-%m-%d')))
    days.append(str((datetime.now() - timedelta(2)).strftime('%Y-%m-%d')))
    days.append(str((datetime.now() - timedelta(3)).strftime('%Y-%m-%d')))
    days.append(str((datetime.now() - timedelta(4)).strftime('%Y-%m-%d')))
    days.append(str((datetime.now() - timedelta(5)).strftime('%Y-%m-%d')))
    days.append(str((datetime.now() - timedelta(6)).strftime('%Y-%m-%d')))
    days.append(str((datetime.now() - timedelta(7)).strftime('%Y-%m-%d')))

    return days
