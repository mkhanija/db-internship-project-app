# DB Internship Project App

## API Definitions
1. ##### Get all data for main **_home_** **_page_**.
    - `http://db-internship-server.herokuapp.com/data-all-companies`
    - The URL above will return a JSON object containing all the data necessary for the home page. The format for the JSON object that will be returned is defined below. NOTE: All dates are formated as YYYY-MM-DD and 'change' in 'company_stocks' is not a decimal but rather a percentage (i.e 0.56 = 0.56%).
    ```
    {
        'app_data':
        [
            {
                'company_name': 'Apple',
                'company_ticker': 'AAPL',
                'company_stocks_last_refreshed': '2019-07-18 13:38:36',
                'company_stocks':
                [
                    {
                        'date': '2019-07-18',
                        'open': '2004.0000',
                        'high': '205.7800',
                        'low': '203.7000',
                        'close': '204.9400',
                        'change': '0.46',
                        'volume': '10459078'
                    }
                ],
                'twitter_sentiment':
                [
                    {
                        'date': '2019-07-18',
                        'sentiment': 
                        [
                            ['overall_sentiment', 'positive'],
                            ['count_positives', 7],
                            ['count_neutrals', 1],
                            ['count_negatives', 2],
                            ['texts_analyzed', 10]
                        ]
                    }
                ]
                'media_sentiment': 'TBD'
            },
            {
                ...
            },
            {
                'company_name': 'Microsoft',
                'company_ticker': 'MSFT',
                'company_stocks_last_refreshed': '2019-07-18 13:38:58',
                'company_stocks':
                [
                    {
                        'date': '2019-07-18',
                        'open': '135.5500',
                        'high': '136.4500',
                        'low': '134.6700',
                        'close': '135.1550',
                        'change': '-0.29',
                        'volume': '13887075'
                    }
                ],
                'twitter_sentiment':
                [
                    {
                        'date': '2019-07-18',
                        'sentiment': 
                        [
                            ['overall_sentiment', 'positive'],
                            ['count_positives', 4],
                            ['count_neutrals', 4],
                            ['count_negatives', 2],
                            ['texts_analyzed', 10]
                        ]
                    }
                ]
                'media_sentiment': 'TBD'
            }
        ]
    }
    ```
    2. ##### Get all data for a specific company within the past 7 days.
    - `http://db-internship-server.herokuapp.com/data-all-companies?ticker=[ticker_ticker]`
    -  The HTTP request will return a JSON object containing all the data necessary for one specific company. The JSON object will contain stock, twitter sentiment, and media sentiment information from the past seven days (excluding weekends). The following format is the result of making a POST request using `http://db-internship-server.herokuapp.com/data-all-companies?ticker=JPM`.
    ```
    {
        'app_data': 
        [
            {
                'company_name': 'JPMorgan Chase',
                'company_ticker': 'JPM',
                'company_stocks_last_refreshed': '2019-07-18 13:41:11',
                'company_stocks':
                [
                    {
                        'date': '2019-07-18',
                        'open': '113.9300',
                        'high': '115.0700',
                        'low': '113.5500',
                        'close': '113.990',
                        'change': '-0.38',
                        'volume': '13120924'
                    }
                    {
                        ...
                    }
                    {
                        'date': '2019-07-12',
                        'open': '114.1300',
                        'high': '115.3500',
                        'low': '113.9300',
                        'close': '115.3000',
                        'change': '1.03',
                        'volume': '10783395'
                    }
                ],
                'twitter_sentiment':
                [
                    {
                        'date': '2019-07-19',
                        'sentiment': 
                        [
                            [overall_sentiment', 'positive'],
                            ['count_positives', 7],
                            ['count_neutrals', 1],
                            ['count_negatives', 2],
                            ['texts_analyzed', 10]
                        ]
                    },
                    {
                        ...
                    },
                    {
                        'date': '2019-07-12',
                        'sentiment': 
                        [
                            [overall_sentiment', 'positive'],
                            ['count_positives', 23],
                            ['count_neutrals', 17],
                            ['count_negatives', 10],
                            ['texts_analyzed', 50]
                        ]
                    }
                ],
                'media_sentiment': 'TBD'
            }
        ]
    }
    ```